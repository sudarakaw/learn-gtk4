use std::sync::OnceLock;

use gtk::{glib, prelude::*, Application, ApplicationWindow, Button};
use tokio::runtime::Runtime;

const APP_ID: &str = "dev.suda.gtk4_ch06";

fn runtime() -> &'static Runtime {
    static RUNTIME: OnceLock<Runtime> = OnceLock::new();

    RUNTIME.get_or_init(|| Runtime::new().expect("Setting up tokio runtime needs to succeed."))
}

fn main() -> glib::ExitCode {
    // Create a new application
    let app = Application::builder().application_id(APP_ID).build();

    // Connect to "activaye" signal of `app`
    app.connect_activate(build_ui);

    // Run the application
    app.run()
}

fn build_ui(app: &Application) {
    // Create a button with label and margins
    let button = Button::builder()
        .label("Press me!")
        .margin_top(12)
        .margin_bottom(12)
        .margin_start(12)
        .margin_end(12)
        .build();

    //Create channel that can hold at most 1 message at a time
    let (tx, rx) = async_channel::bounded(1);

    // Connect to "clicked" signal of `button`
    button.connect_clicked(move |_| {
        // The main loop executes the asynchronous block
        runtime().spawn(glib::clone!(@strong tx => async move {
            let response=reqwest::get("https://www.gtk-rs.org").await;

            tx.send(response).await.expect("The channel needs to be open.");
        }));
    });

    // The main loop executes the asynchronous block
    glib::spawn_future_local(async move {
        while let Ok(res) = rx.recv().await {
            if let Ok(res) = res {
                println!("Status: {}", res.status());
            } else {
                println!("Could no make a `GET` request.");
            }
        }
    });

    // Create window and set the title
    let window = ApplicationWindow::builder()
        .application(app)
        .title("My GTK App")
        .child(&button)
        .build();

    // Present window
    window.present();
}
