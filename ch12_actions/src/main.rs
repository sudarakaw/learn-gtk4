mod window;

use gtk::{
    gio::{self, ActionEntry},
    glib,
    prelude::*,
    Application,
};
use window::Window;

const APP_ID: &str = "dev.suda.gtk4_ch12";

fn main() -> glib::ExitCode {
    // Register and include resources
    gio::resources_register_include!("app.gresource").expect("Failed to register resources.");

    // Create a new application
    let app = Application::builder().application_id(APP_ID).build();

    // Connect to "activaye" signal of `app`
    app.connect_activate(build_ui);

    // Set keyboard accelerator to trigger "win.close"
    app.set_accels_for_action("win.close", &["<Ctrl>W"]);

    // Run the application
    app.run()
}

fn build_ui(app: &Application) {
    // Create window and present it
    let window = Window::new(app);

    // Add action "close" to `window` taking no parameter
    let action_close = ActionEntry::builder("close")
        .activate(|window: &Window, _, _| {
            window.close();
        })
        .build();

    window.add_action_entries([action_close]);

    // Present window
    window.present();
}
