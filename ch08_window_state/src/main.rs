mod custom_window;

use custom_window::Window;
use gtk::{glib, prelude::*, Application, Button};

const APP_ID: &str = "dev.suda.gtk4_ch08";

fn main() -> glib::ExitCode {
    // Create a new application
    let app = Application::builder().application_id(APP_ID).build();

    // Connect to "activaye" signal of `app`
    app.connect_activate(build_ui);

    // Run the application
    app.run()
}

fn build_ui(app: &Application) {
    // Create a window
    let window = Window::new(app);

    // Create a button with label and margins
    let button = Button::builder()
        .label("Press me!")
        .margin_top(12)
        .margin_bottom(12)
        .margin_start(12)
        .margin_end(12)
        .build();

    // Connect to "clicked" signal of `button`
    button.connect_clicked(|button| {
        // Change the button label
        button.set_label("Hello World!!!");
    });

    window.set_child(Some(&button));

    // Present window
    window.present();
}
