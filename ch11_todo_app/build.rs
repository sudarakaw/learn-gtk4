fn main() {
    glib_build_tools::compile_resources(
        &["resources"],
        "resources/resource.gresource.xml",
        "app.gresource",
    );
}
