mod window;

// use gtk::{gdk::Display, glib, prelude::*, Application, ApplicationWindow, Button, CssProvider};
use gtk::{gdk::Display, glib, prelude::*, Application, CssProvider};
use window::Window;

const APP_ID: &str = "dev.suda.gtk4_ch14";

fn main() -> glib::ExitCode {
    // Create a new application
    let app = Application::builder().application_id(APP_ID).build();

    // Connect to signals
    app.connect_startup(|_| load_css());
    app.connect_activate(build_ui);

    // Run the application
    app.run()
}

fn load_css() {
    // Load the CSS file and add it to the provider
    let provider = CssProvider::new();

    provider.load_from_string(include_str!("./style.css"));

    // Add the provider to the default screen
    gtk::style_context_add_provider_for_display(
        &Display::default().expect("Could not connect to a display."),
        &provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );
}

fn build_ui(app: &Application) {
    // Create a buttons
    // let button_1 = Button::with_label("Destructive");
    // let button_2 = Button::with_label("Suggested");
    //
    // // button_1.add_css_class("button-1");
    // button_1.add_css_class("destructive-action");
    // button_2.add_css_class("suggested-action");
    //
    // // Create `gtk_box` and add buttons
    // let gtk_box = gtk::Box::builder()
    //     .margin_top(12)
    //     .margin_bottom(12)
    //     .margin_start(12)
    //     .margin_end(12)
    //     .spacing(12)
    //     .build();
    //
    // gtk_box.append(&button_1);
    // gtk_box.append(&button_2);
    //
    // // Create window and set the title
    // let window = ApplicationWindow::builder()
    //     .application(app)
    //     .title("My GTK App")
    //     .child(&gtk_box)
    //     .build();

    let window = Window::new(app);

    // Present window
    window.present();
}
