use std::cell::Cell;

use gtk::{
    glib::{
        self,
        subclass::{
            object::{ObjectImpl, ObjectImplExt},
            types::{ObjectSubclass, ObjectSubclassExt},
        },
    },
    prelude::ButtonExt,
    subclass::{button::ButtonImpl, widget::WidgetImpl},
};

// Object holding the state
#[derive(Default)]
pub struct CustomButton {
    number: Cell<i32>,
}

#[glib::object_subclass]
impl ObjectSubclass for CustomButton {
    const NAME: &'static str = "MyGtkCustomButton";

    type Type = super::CustomButton;
    type ParentType = gtk::Button;
}

// Trait shared by all GObjects
impl ObjectImpl for CustomButton {
    fn constructed(&self) {
        self.parent_constructed();

        self.obj().set_label(&self.number.get().to_string());
    }
}

// Trait shared by all widgets
impl WidgetImpl for CustomButton {}

// Trait shared by all buttons
impl ButtonImpl for CustomButton {
    fn clicked(&self) {
        self.number.set(self.number.get() + 1);
        self.obj().set_label(&self.number.get().to_string());
    }
}
