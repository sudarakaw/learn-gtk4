mod task_object;
mod task_row;
mod utils;
mod window;

use adw::{prelude::*, Application};
// use gtk::{prelude::*, Application};
use gtk::{
    gdk::Display,
    gio::{self, ActionEntry},
    glib, CssProvider,
};

use window::Window;

const APP_ID: &str = "dev.suda.gtk4_todo";

fn main() -> glib::ExitCode {
    // Register and include resources
    gio::resources_register_include!("app.gresource").expect("Failed to register resources.");

    // Create a new application
    let app = Application::builder().application_id(APP_ID).build();

    // Connect to signals
    app.connect_startup(|app| {
        setup_shortcurs(app);
        load_css()
    });
    app.connect_activate(build_ui);

    // Run the application
    app.run()
}

fn load_css() {
    // Load the CSS file and add it to the provider
    let provider = CssProvider::new();
    provider.load_from_resource("/dev/suda/gtk4_todo/style.css");

    // Add the provider to the default screen
    gtk::style_context_add_provider_for_display(
        &Display::default().expect("Could not connect to a display."),
        &provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );
}

fn setup_shortcurs(app: &Application) {
    app.set_accels_for_action("win.filter('All')", &["<Ctrl>a"]);
    app.set_accels_for_action("win.filter('Open')", &["<Ctrl>o"]);
    app.set_accels_for_action("win.filter('Done')", &["<Ctrl>d"]);

    // Set keyboard accelerator to trigger "win.close"
    app.set_accels_for_action("win.close", &["<Ctrl>W"]);
}

fn build_ui(app: &Application) {
    // Create new window and present it
    let window = Window::new(app);

    // Add action "close" to `window` taking no parameter
    let action_close = ActionEntry::builder("close")
        .activate(|window: &Window, _, _| {
            window.close();
        })
        .build();

    window.add_action_entries([action_close]);

    window.present();
}
