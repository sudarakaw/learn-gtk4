use gtk::{gio::Settings, glib, prelude::*, Align, Application, ApplicationWindow, Switch};

const APP_ID: &str = "dev.suda.gtk4_ch07";

fn main() -> glib::ExitCode {
    // Create a new application
    let app = Application::builder().application_id(APP_ID).build();

    // Connect to "activaye" signal of `app`
    app.connect_activate(build_ui);

    // Run the application
    app.run()
}

fn build_ui(app: &Application) {
    // Initialize Settings
    let settings = Settings::new(APP_ID);

    // Get the last switch state from settings
    // let is_switch_enabled = settings.boolean("is-switch-enabled");

    // Create a switch
    let switch = Switch::builder()
        .margin_top(48)
        .margin_bottom(48)
        .margin_start(48)
        .margin_end(48)
        .valign(Align::Center)
        .halign(Align::Center)
        // .state(is_switch_enabled)
        .build();

    settings
        .bind("is-switch-enabled", &switch, "active")
        .build();

    // switch.connect_state_set(move |_, is_enabled| {
    //     // Save changed switch state in the settings
    //     settings
    //         .set_boolean("is-switch-enabled", is_enabled)
    //         .expect("Could no set settings.");
    //
    //     // Allow to invoke other event handlers
    //     glib::Propagation::Proceed
    // });

    // Create window and set the title
    let window = ApplicationWindow::builder()
        .application(app)
        .title("My GTK App")
        .child(&switch)
        .build();

    // Present window
    window.present();
}
