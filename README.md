# GUI development with Rust and GTK 4 - Code Samples

Practice code written while following "GUI development with Rust and GTK 4" book.

- GTK v4.12
- Book [GUI development with Rust and GTK 4](https://gtk-rs.org/gtk4-rs/stable/latest/book/)

