mod custom_button;

use custom_button::CustomButton;
use gtk::{
    glib::{self, closure_local},
    prelude::*,
    Align, Application, ApplicationWindow, Orientation,
};

const APP_ID: &str = "dev.suda.gtk4_ch05_d";

fn main() -> glib::ExitCode {
    // Create a new application
    let app = Application::builder().application_id(APP_ID).build();

    // Connect to "activaye" signal of `app`
    app.connect_activate(build_ui);

    // Run the application
    app.run()
}

fn build_ui(app: &Application) {
    // Create a button
    let button_1 = CustomButton::new();
    let button_2 = CustomButton::new();

    // Assure that "number" of `button_2` is always 1 higher than "number" of `button_1`
    button_1
        .bind_property("number", &button_2, "number")
        // How to transform "number" from `button_1` to "number" of `button_2`
        .transform_to(|_, number: i32| {
            let incremented_number = number + 1;
            Some(incremented_number.to_value())
        })
        // How to transform "number" from `button_2` to "number" of `button_1`
        .transform_from(|_, number: i32| {
            let decremented_number = number - 1;
            Some(decremented_number.to_value())
        })
        .bidirectional()
        .sync_create()
        .build();

    // The closure will be called whenever the property "number" of `button_1`
    // gets changed
    button_1.connect_number_notify(|button| {
        println!("The current number of `button_1` is {}", button.number());
    });

    button_1.connect_closure(
        "max-number-reached",
        false,
        closure_local!(move |_: CustomButton, number: i32| {
            println!("The maximum number {} has been reached", number);
        }),
    );

    // Set up box
    let gtk_box = gtk::Box::builder()
        .margin_top(12)
        .margin_end(12)
        .margin_end(12)
        .margin_start(12)
        .valign(Align::Center)
        .halign(Align::Center)
        .spacing(12)
        .orientation(Orientation::Vertical)
        .build();

    gtk_box.append(&button_1);
    gtk_box.append(&button_2);

    // Create window and set the title
    let window = ApplicationWindow::builder()
        .application(app)
        .title("My GTK App")
        .child(&gtk_box)
        .build();

    // Present window
    window.present();
}
